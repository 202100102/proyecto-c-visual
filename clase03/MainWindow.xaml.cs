﻿using clase03.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace clase03
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Usuario> usuarios;
        private Usuario usuarioEditando;
        public MainWindow()
        {
            InitializeComponent();
            CargarUsuarios();


        }

        public void CargarUsuarios()
        {
            DataBaseContext conexion = new DataBaseContext();
            usuarios = conexion.Usuarios.ToList();
            tblUsuarios.ItemsSource = usuarios;
            conexion.Dispose();

        }

        private void ButtonGuardar(object sender, RoutedEventArgs e)
        {
            Usuario usuario = new Usuario();

            usuario.Id = 0;
            usuario.Nombres = txtNombres.Text;
            usuario.Apellidos = txtApellidos.Text;
            usuario.Correo = txtCorreo.Text;
            usuarios.Add(usuario);
            tblUsuarios.Items.Refresh();

            DataBaseContext conexion = new DataBaseContext();
            conexion.Usuarios.Add(usuario);
            conexion.SaveChanges();
            conexion.Dispose();
        }

        private void ButtonActualizar(object sender, RoutedEventArgs e)
        {
            if(usuarioEditando == null)
            {
                return;
            }
            Usuario usuario = usuarioEditando; //usando el usuario seleccionado

            //usuario.Id = 0;
            usuario.Nombres = txtNombres.Text;
            usuario.Apellidos = txtApellidos.Text;
            usuario.Correo = txtCorreo.Text;
            //usuarios.Add(usuario);
            tblUsuarios.Items.Refresh();

            DataBaseContext conexion = new DataBaseContext();
            conexion.Usuarios.Update(usuario); //modo de actualizancion en bd
            conexion.SaveChanges();
            conexion.Dispose();
            usuarioEditando = null; //eliminar edicion
        }


        private void tblUsuarios_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Delete)
            {
                DataBaseContext conexion = new DataBaseContext();
                for(int i = 0; i < tblUsuarios.SelectedItems.Count; i++)
                {
                    Usuario uTemporal = (Usuario)tblUsuarios.SelectedItems[i];
                    conexion.Usuarios.Remove(uTemporal);
                }
                conexion.SaveChanges();
                conexion.Dispose();
            }

        }

        private void tblUsuarios_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            usuarioEditando = (Usuario)tblUsuarios.SelectedItem;
            if(usuarioEditando == null)
            {
                return;
            }
            txtNombres.Text = usuarioEditando.Nombres;
            txtApellidos.Text = usuarioEditando.Apellidos;
            txtCorreo.Text = usuarioEditando.Correo;

        }
    }
}
